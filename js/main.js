// 109 is the default size in px, we want 350
const svgScale = 350.0/109.0;
// Used to scale the svg to the above scale
const pathTransform = document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGMatrix().scale(svgScale);
const urlParams = new URLSearchParams(window.location.search);
let urlChar = urlParams.get('k');
let xhttp = new XMLHttpRequest();
var curChars = '';
// What char we're drawing
var curCharInd = 0;
// For looping through our svg requests
var requested = 0;
var strokes = [];
var totalStrokes = 0;
// kanji characters in the background
var bgChars = [];
// Canvases
var cvs = [];
// canvas contexts
var ctx = [];
// A container for our canvas drawing as svg (using svgcanvas)
var csvg = new Context(350, 350);
var curStroke = 0;
var pos = { x: 0, y: 0 };
var doneStrokes = [];
var errorStroke = null;
var busy = false;
// the animation overlay (drawing tutorial)
var animOver = SVG().addTo('#anim-over').size(350, 350);
var convertFinished = false;
var showOrder = false;
var overlay = true;
var hinterval = null;

// What stroke are we on in the current kanji?
function getCurrentStroke() {
    let curStrokeTmp = curStroke;
    for(let i = 0; i < strokes.length; i++) {
        if (curStrokeTmp < strokes[i]) return curStrokeTmp;
        curStrokeTmp -= strokes[i];
    }
    // End of array
    return 0;
}

// What kanji index are we at?
function getCurrentChar() {
    let curStrokeTmp = curStroke;
    for(let i = 0; i < strokes.length; i++) {
        if (curStrokeTmp < strokes[i]) return i;
        curStrokeTmp -= strokes[i];
    }
    // End of array
    return strokes.length-1;
}

// Moves our absolute overlays to cheat their position because I'm lazy
function moveOverlay() {
    let rect = id("cvs-area").getBoundingClientRect();
    let cdraws = document.querySelectorAll(".cdraw");
    if(cdraws[curCharInd]) {
        let cur = cdraws[curCharInd].getBoundingClientRect();
        id("anim-over").style.left = ((cur.width/2.0)-(350.0/2.0)+cur.left)+"px";
        id("grid-under").style.left = ((cur.width/2.0)-(350.0/2.0)+cur.left)+"px";
    } else {
        id("anim-over").style.left = ((rect.width/2.0)-(350.0/2.0)+rect.left)+"px";
        id("grid-under").style.left = ((rect.width/2.0)-(350.0/2.0)+rect.left)+"px";
    }
}

// Get a kanji SVG from our text at index
function requestChar(ind) {
    let code = entityUnicode(curChars[ind]);
    xhttp.open("GET", `kanjivg/kanji/${code}.svg`, true);
    xhttp.send();
}

// Page load
(() => {
    console.log("v1.1.2");
    moveOverlay();
    id("chk-overlay").checked = true;
    id("chk-convert").checked = false;
    id("chk-order").checked = false;
    if(urlChar) {
        curChars = urlChar.trim();
        requested = 0;
        requestChar(0);
        id("chars").value = curChars;
    }
})();

window.onresize = moveOverlay;

function id(s) {
    return document.getElementById(s);
}

// Converts text to a unicode value string
function entityUnicode(txt) {
    var code = txt.charCodeAt(0);
    var codeHex = code.toString(16);
    while (codeHex.length <= 4) {
        codeHex = "0" + codeHex;
    }
    return codeHex;
}

// Converts plaintext matrix data in an SVG into xy position at scale
function matrixXY(m) {
    m = m.replace(')', '');
    let spl = m.split(' ');
    let x = parseFloat(spl[4]);
    let y = parseFloat(spl[5]);
    if(x === null || x === NaN || y === null || y === NaN) return null;
    return {x: x * svgScale, y: y * svgScale};
}

// Remove canvases, start new
function removeCvs() {
    let its = document.querySelectorAll(".cdraw");
    if(its === null || its.length === 0) return;
    its.forEach(it => {
        it.remove();
    });
    cvs = [];
    ctx = [];
    bgChars = [];
    doneStrokes = [];
    strokes = [];
    totalStrokes = 0;
}

// user entered new char
function enter() {
    if(hinterval !== null) {
        clearInterval(hinterval);
        animOver.clear();
        id("anim-over").style.visibility = 'hidden';
        hinterval = null;
        busy = false;
    }
    let trimmed = id("chars").value.trim();
    let shouldPush = trimmed !== curChars && trimmed != '';
    curChars = trimmed;
    if(shouldPush)
        window.history.pushState({}, "", window.location.href.split("?")[0]+"?k="+curChars);
    removeCvs();
    requested = 0;
    requestChar(0);
}

// Canvas mouse position for drawing
function setMousePos(e) {
    if (e.target.dataset.index !== `${curCharInd}`) return;
    let bb = document.querySelectorAll(".cdraw")[curCharInd].getBoundingClientRect();
    pos.x = e.clientX - bb.left;
    pos.y = e.clientY - bb.top;
}

// Canvas enter event
function cvsEnter(e) {
    if (e.target.dataset.index !== `${curCharInd}`) return;
    ctx[curCharInd].strokeStyle = '#ffffff';
    setMousePos(e);
}

// This stroke makes the user feel incorrect
function setErrorStroke(svg) {
    if(svg.node.attributes.d === null) return;
    errorStroke = new Path2D(svg.node.attributes.d.value);
}

// A stroke was completed (And will be redrawn until removed)
function addDoneStroke(svg) {
    if(svg.node.attributes.d === null) return;
    doneStrokes[curCharInd].push(new Path2D(svg.node.attributes.d.value));
}

// Mouse up in canvas
function cvsUp(e) {
    if(busy || e.target.dataset.index !== `${curCharInd}` || curStroke === totalStrokes) return;
    // get stroke as svg for comparisons
    let svgStroke = SVG(csvg.getSvg()).find('path[stroke="#000000"]')[0];
    //console.log(SVG(csvg.getSvg()).find('path'));
    if(svgStroke === null || svgStroke === undefined) return;

    let realStroke = bgChars[curCharInd].find('path')[getCurrentStroke()];
    let failed = false;

    // compare lengths to not be too off
    //console.log(`p ${svgStroke.length()} ${realStroke.length()*svgScale}`);
    if(Math.abs(svgStroke.length() - (realStroke.length()*svgScale)) > 50.0) {
        // failed stroke
        failed = true;
    }

    // Compare average distance of user stroke shape to svg stroke shape
    if(!failed) {
        let avg = 0.0;
        let len = realStroke.length();
        let scale = svgStroke.length() / realStroke.length();
        for(let i = 0; i < len; i += 4) {
            let sp = svgStroke.pointAt(i * scale);
            let rp = realStroke.pointAt(i);
            let plus = Math.abs((rp.x * svgScale) - sp.x) + Math.abs((rp.y * svgScale) - sp.y);
            avg += plus;
        }
        //console.log(avg / (len * 0.25));
        if(avg / (len * 0.25) > 17.0) {
            failed = true;
        }
    }

    if(!failed) {
        curStroke += 1;
        addDoneStroke(svgStroke);
        curCharInd = getCurrentChar();
        moveOverlay();
        if(convertFinished) redraw();
    } else {
        busy = true;
        setErrorStroke(svgStroke);
        redraw();
        setTimeout(() => {
            busy = false;
            errorStroke = null;
            redraw();
        }, 800);
    }

    csvg.clearRect(0,0,9999,9999);

    ctx[curCharInd].beginPath(); // begin
    csvg.beginPath();
}
  
function draw(e) {
    // Left button must be pressed, only allow current kanji index to be drawn, don't draw when completed
    if(e.buttons !== 1 || e.target.dataset.index !== `${curCharInd}` || curStroke === totalStrokes) return;
    ctx[curCharInd].strokeStyle = '#ffffff';
  
    if(!busy) {
        ctx[curCharInd].moveTo(pos.x, pos.y); // from
        csvg.moveTo(pos.x, pos.y);
    }
    setMousePos(e);
    if(!busy) {
        ctx[curCharInd].lineTo(pos.x, pos.y); // to
        csvg.lineTo(pos.x, pos.y);
    
        ctx[curCharInd].stroke(); // draw it!
        csvg.stroke();
    }
}

function clearState() {
    curCharInd = 0;
    curStroke = 0;
    errorStroke = null;
}

// Draw our canvases again (And maybe hint at a stroke [yellow])
function redraw(hint) {
    if(ctx.length === 0) return;
    let ind = 0;
    ctx.forEach((c, cind) => {
        c.clearRect(0,0,9999,9999);
        bgChars[cind].find('path').forEach(el => {
            if(el.node.attributes.d === null) return;
            let draw = true;
            if(hint !== undefined && ind === hint) {
                c.strokeStyle = '#ffc107';
            } else if(convertFinished && ind < curStroke) {
                c.strokeStyle = '#ffffff';
            } else {
                c.strokeStyle = '#777777';
                draw = overlay;
            }
            if(draw) {
                let path = new Path2D(el.node.attributes.d.value);
                let ins = new Path2D();
                ins.addPath(path, pathTransform);
                c.beginPath(); // begin
                c.stroke(ins);
            }
            ind += 1;
        });
        if(showOrder) {
            c.font = '24px serif';
            c.strokeStyle = '#777777';
            c.fillStyle = '#777777';
            bgChars[cind].find("text").forEach((el) => {
                let pos = matrixXY(el.node.attributes.transform.value);
                c.fillText(el.node.innerHTML, pos.x, pos.y);
            });
        }
        if(!convertFinished) {
            c.strokeStyle = '#ffffff';
            doneStrokes[cind].forEach((s) => {
                c.beginPath(); // begin
                c.stroke(s);
            });
        }
        if(errorStroke !== null && curCharInd == cind) {
            c.strokeStyle = '#dc3545';
            c.beginPath(); // begin
            c.stroke(errorStroke);
        }
        c.strokeStyle = '#ffffff';
    });
    if(ctx[curCharInd])
        ctx[curCharInd].beginPath(); // begin
    csvg.beginPath();
}

// Animate the drawing tutorial for {stroke}
function animHintPath(stroke) {
    let hstroke = bgChars[curCharInd].find('path')[stroke];
    animOver.path(hstroke.node.attributes.d.value)
        .attr({
            fill: 'none',
            stroke: '#ffc107',
            'stroke-width': 3.2,
            'stroke-linecap': 'round',
            class: 'autodraw',
            'stroke-dasharray': hstroke.length(),
            'stroke-dashoffset': hstroke.length()
        })
        .scale(svgScale, svgScale, 0, 0);
}

// Creates a new canvas for use on the page (added to page later)
function createCanvas(ind) {
    let canvs = document.createElement('canvas');
    canvs.className = "cdraw";
    canvs.dataset.index = `${ind}`;

    let c = canvs.getContext('2d');
    c.canvas.width = 350;
    c.canvas.height = 350;
    c.beginPath(); // begin
    c.lineWidth = 9;
    c.lineCap = 'round';
    c.strokeStyle = '#777777';
    ctx.push(c);
    cvs.push(canvs);
    return canvs;
}

xhttp.onreadystatechange = function() {
    if(this.readyState === 4) {
        if(this.status === 200) {
            // We got a kanji svg
            let area = id("cvs-area");
            id("btn-info").removeAttribute("disabled");

            let canvs = createCanvas(bgChars.length);

            area.onmousemove = draw;
            area.onmousedown = setMousePos;
            area.onmouseup = cvsUp;
            area.onmouseenter = cvsEnter;
            let newBg = SVG(xhttp.responseXML);
            let newLen = newBg.find('path').length;
            totalStrokes += newLen;
            strokes.push(newLen);
            bgChars.push(newBg);
            doneStrokes.push([]);
            //console.log(bgChar);

            clearState();

            redraw();

            area.appendChild(canvs);
            moveOverlay();
        } else if(strokes.length === 0) {
            id("btn-info").setAttribute("disabled", "");
            curChars = '';
            img.src = '';
            alert(`Could not find character (${this.status})`);
        }
        // Loop through if we have more characters (One at a time)
        requested += 1;
        if(requested < 6 && requested < curChars.length) {
            requestChar(requested);
        }
    }
};

id("btn-enter").onclick = enter;

id("chars").onkeydown = function(e) {
    if(e.key === "Enter") {
        enter();
    }
};

id("btn-hide").onclick = function() {
    if(id("chars").getAttribute("type") === "text") {
        id("chars").setAttribute("type", "password");
    } else {
        id("chars").setAttribute("type", "text");
    }
}

id("btn-info").onclick = function() {
    // todo: More chars?
    if(curChars[curCharInd])
        window.open(`https://www.kakimashou.com/dictionary/character/${curChars[curCharInd]}`, "_blank");
}

id("btn-clear").onclick = function() {
    if(ctx.length === 0) return;
    clearState();

    ctx.forEach(c => {
        c.clearRect(0,0,9999,9999);
    });

    for(let i = 0; i < doneStrokes.length; i++) {
        doneStrokes[i] = [];
    }

    redraw();
    moveOverlay();
}

id("btn-hint").onclick = function() {
    if(ctx.length === 0 || busy) return;
    redraw(curStroke);
    busy = true;
    setTimeout(() => {
        busy = false;
        redraw();
    }, 800);
}

id("btn-show").onclick = function() {
    let curRelative = getCurrentStroke();
    let len = bgChars[curCharInd].find('path').length - curRelative;
    if(len === 0 || busy || curStroke === totalStrokes) return;
    var hint = 0;

    busy = true;
    id("anim-over").style.visibility = 'visible';
    // We animate a line, wait 1 second, and then do the next line
    // see: .autodraw css class
    animHintPath(curRelative);
    hinterval = setInterval(() => {
        hint += 1;
        if(hint > len) {
            clearInterval(hinterval);
            animOver.clear();
            id("anim-over").style.visibility = 'hidden';
            hinterval = null;
            busy = false;
        } else if(hint < len) {
            animHintPath(curRelative + hint);
        }
    }, 1000);
}

id("chk-overlay").onchange = function() {
    overlay = id("chk-overlay").checked;
    redraw();
}

id("chk-convert").onchange = function() {
    convertFinished = id("chk-convert").checked;
    redraw();
}

id("chk-order").onchange = function() {
    showOrder = id("chk-order").checked;
    redraw();
}
